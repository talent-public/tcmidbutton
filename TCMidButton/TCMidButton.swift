//
//  TCMidButton.swift
//  CustomControl
//
//  Created by Peerasak Unsakon on 18/12/2563 BE.
//

import UIKit

@IBDesignable
public class TCMidButton: UIControl {
    
    @IBInspectable public var image: UIImage? {
        didSet {
            self.setupButton()
        }
    }
    
    @IBInspectable public var highlightedImage: UIImage? {
        didSet {
            self.setupButton()
        }
    }
    
    @IBInspectable public var selectedImage: UIImage? {
        didSet {
            self.setupButton()
        }
    }
    
    @IBInspectable public var disabledImage: UIImage? {
        didSet {
            self.setupButton()
        }
    }
    
    
    @IBInspectable public var imageHeight: CGFloat = 60 {
        didSet {
            self.setupButton()
        }
    }
    
    @IBInspectable public var titleFont: UIFont = .systemFont(ofSize: 14) {
        didSet {
            self.setupButton()
        }
    }
    
    @IBInspectable public var title: String = "" {
        didSet {
            self.setupButton()
        }
    }
    
    @IBInspectable public var highlightedTitle: String? {
        didSet {
            self.setupButton()
        }
    }
    
    @IBInspectable public var selectedTitle: String? {
        didSet {
            self.setupButton()
        }
    }
    
    @IBInspectable public var disabledTitle: String? {
        didSet {
            self.setupButton()
        }
    }
    
    public override var isSelected: Bool {
        didSet {
            self.setupButton()
        }
    }
    
    public override var isEnabled: Bool {
        didSet {
            self.setupButton()
        }
    }
    
    public var imageView: UIImageView!
    public var titleLabel: UILabel!
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupButton()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupButton()
    }
    
    public convenience init(image: UIImage?, title: String, frame: CGRect = .zero) {
        self.init(frame: frame)
        self.title = title
        self.image = image
        self.setupButton()
    }
    
    private func addContentView() {
        if self.imageView == nil {
            self.imageView = UIImageView()
            self.imageView.contentMode = .scaleAspectFit
            addSubview(self.imageView)
            
            self.titleLabel = UILabel()
            self.titleLabel.textAlignment = .center
            addSubview(self.titleLabel)
        }
    }
    
    private func setTitle() {
        self.titleLabel.font = titleFont
        self.titleLabel.alpha = 1.0
        self.titleLabel.numberOfLines = 2
        
    
        self.titleLabel.text = self.title

        if !self.isEnabled {
            self.titleLabel.alpha = 0.6
            self.titleLabel.text = self.disabledTitle ?? self.title
        }

        if self.isHighlighted {
            self.titleLabel.text = self.highlightedTitle ?? self.title
        }

        if self.isSelected {
            self.titleLabel.text = self.selectedTitle ?? self.title
        }
        
    }
    
    private func setImage() {
        
        self.imageView.image = self.image
        
        if !self.isEnabled {
            self.imageView.image = self.disabledImage ?? self.image
        }

        if self.isHighlighted {
            self.imageView.image = self.highlightedImage ?? self.image
        }

        if self.isSelected {
            self.imageView.image = self.selectedImage ?? self.image
        }
        
    }
    
    private func setupButton() {
        self.addContentView()
        
        guard let imageView = self.imageView, let titleLabel = self.titleLabel else { return }
        
        self.setImage()
        self.setTitle()
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.removeAllConstraints()
        
        addConstraints(NSLayoutConstraint.constraints(
                                withVisualFormat: "H:|-[imageView]-|",
                                options: [.directionLeadingToTrailing],
                                metrics: nil,
                                views: ["imageView": imageView]))
        
        addConstraints(NSLayoutConstraint.constraints(
                                withVisualFormat: "H:|-0-[label]-0-|",
                                options: .directionLeadingToTrailing,
                                metrics: nil,
                                views: ["label": titleLabel]))
        
        let metrics = [
            "imageHeight": self.imageHeight + (self.isSelected ? 10 : 0)
        ]
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[imageView(imageHeight)]-0-[label]-0-|", options: [.alignAllCenterX], metrics: metrics, views: ["imageView": imageView, "label": titleLabel]))
        
        addTarget(self, action: #selector(self.addHighlight), for: [.touchDown, .touchDragEnter])
        addTarget(self, action: #selector(self.removeHighlight), for: [.touchUpInside, .touchDragExit])
    }
    
    @objc func addHighlight() {
        self.setTitle()
        self.setImage()
    }
    
    @objc func removeHighlight() {
        self.setTitle()
        self.setImage()
    }
    
}

extension UIView {
    
    public func removeAllConstraints() {
        var _superview = self.superview
        
        while let superview = _superview {
            for constraint in superview.constraints {
                
                if let first = constraint.firstItem as? UIView, first == self {
                    superview.removeConstraint(constraint)
                }
                
                if let second = constraint.secondItem as? UIView, second == self {
                    superview.removeConstraint(constraint)
                }
            }
            
            _superview = superview.superview
        }
        
        self.removeConstraints(self.constraints)
        self.translatesAutoresizingMaskIntoConstraints = true
    }
}
