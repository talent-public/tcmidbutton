//
//  ViewController.swift
//  TCMidButtonExample
//
//  Created by Peerasak Unsakon on 9/2/2564 BE.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var roseButton: TCMidButton!
    @IBOutlet weak var jennieButton: TCMidButton!
    @IBOutlet weak var jisooButton: TCMidButton!
    @IBOutlet weak var lisaButton: TCMidButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonTapped(_ sender: TCMidButton) {
        let buttons = [roseButton, jennieButton, jisooButton, lisaButton]
        buttons.forEach { (button) in
            button?.isSelected = button == sender
        }
    }
    
}

